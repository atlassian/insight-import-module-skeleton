## Mirroring the Import-Skeleton repository


###### Open Terminal.

###### Create a bare clone of the repository.

*$ git clone --bare https://bitbucket.riada.io/scm/di/insight-import-module-skeleton.git*


###### Mirror-push to the new repository.

*$ cd insight-import-module-skeleton.git*

*$ git push --mirror https://bitbucket.riada.io/scm/ii/new-repository.git*


###### Remove the temporary local repository you created in step 1.

*$ cd ..*

*$ sudo rm -rf insight-import-module-skeleton.git*

###### Set a clean master initial branch

*$ git clone https://bitbucket.riada.io/scm/ii/new-repository.git*

*$ cd new import module*

*$ git checkout --orphan new-master master*

*$ git commit -a --allow-empty-message -m ''*

*$ git branch -M new-master master*

*$ git push --force*

:+1:

---

## Regular build

`atlas-clean; atlas-mvn package`

---

## Demo Mode build

`atlas-clean; atlas-mvn package -Ddemomode=true -DskipTests=true`