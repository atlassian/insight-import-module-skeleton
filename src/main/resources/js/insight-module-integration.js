AJS.$(document).ready(function() {
	AJS.$(".js-license-edit").click(function(e) {
		e.preventDefault()
		AJS.$("#license-edit-info").show();
		AJS.$("#license-show-info").hide();
	});

	AJS.$(".js-rlabs-cancel").click(function(e) {
		e.preventDefault()
		AJS.$("#license-edit-info").hide();
		AJS.$("#license-show-info").show();
		AJS.$("#licenseKey").val(AJS.$(".js-license-complete").html());
	});

	AJS.$(".js-rlabs-save").click(function(e) {
		e.preventDefault()
		AJS.$("#licenseform").submit();
	});

	AJS.$(".js-license-show").click(function(e) {
		e.preventDefault()
		AJS.$(this).hide();
		AJS.$(".js-license-complete").show();
	});
});