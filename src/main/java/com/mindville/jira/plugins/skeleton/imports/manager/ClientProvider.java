package com.mindville.jira.plugins.skeleton.imports.manager;

import com.mindville.jira.plugins.skeleton.imports.ImportConfiguration;
import com.mindville.jira.plugins.skeleton.imports.model.skeleton.SkeletonClient;
import io.riada.CacheProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Supplier;

public enum ClientProvider {
    INSTANCE;

    private final Logger logger = LoggerFactory.getLogger(ClientProvider.class);
    private static String credentials = null; // This will be your credential class from your destination API
    private ImportConfiguration configuration;

    public synchronized SkeletonClient getClient(ImportConfiguration configuration) {
        this.configuration = configuration;

        SkeletonClient result = (SkeletonClient) CacheProvider.getInstance()
                .get(String.valueOf(configuration.hashCode()), new ConfigurationLoader(configuration));

        return result;
    }

    public synchronized boolean validClient(ImportConfiguration configuration) {
        boolean ret = false;

        try {
            if (configuration.isDemoData()) {
                return true;
            }

            SkeletonClient skeletonClient = getClient(configuration);
            if (skeletonClient != null) {

                ret = skeletonClient.isValid();
            }
        } catch (Exception ex) {
            logger.error("Error validating client.", ex);
        }
        return ret;
    }

    public static class ConfigurationLoader implements Supplier<SkeletonClient> {

        private final Logger logger = LoggerFactory.getLogger(ConfigurationLoader.class);

        private final ImportConfiguration configuration;

        public ConfigurationLoader(ImportConfiguration configuration) {
            this.configuration = configuration;

        }

        @Override
        public SkeletonClient get() {
            SkeletonClient skeletonClient = null;
            try {
                if (credentials == null) {
                    setCredentials();
                }

                skeletonClient = new SkeletonClient();

            } catch (Exception ex) {
                logger.error("Error trying to authenticate client.", ex);
            }
            return skeletonClient;
        }

        private void setCredentials() {
            // Maybe you need to set some credentials based on the configuration
            credentials = configuration.getExampleField();
        }

    }

}
