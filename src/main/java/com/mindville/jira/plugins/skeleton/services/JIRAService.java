package com.mindville.jira.plugins.skeleton.services;

import java.util.Date;

public abstract interface JIRAService {

    public static final String ISO8061 = "jira.date.time.picker.use.iso8061";

    public abstract String getBaseURL();

    public abstract String getDateFormat(Date date);

    public abstract String getDateTimeFormat(Date date);

}