package com.mindville.jira.plugins.skeleton.imports;

import com.atlassian.adapter.jackson.ObjectMapper;
import com.google.common.base.Objects;
import com.mindville.jira.plugins.skeleton.util.I18FactoryFake;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportModuleConfiguration;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.insightfield.InsightFieldConfiguration;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.insightfield.text.InsightFieldTextConfiguration;
import io.riada.CacheProvider;
import io.riada.FakeDataProvider;
import io.riada.MavenOutputProperties;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class ImportConfiguration implements ImportModuleConfiguration {

    private static final long serialVersionUID = -4473533132810886240L;

    private final Logger logger = LoggerFactory.getLogger(ImportConfiguration.class);

    private final int cacheExpiration = 5;
    private boolean demoData = false;
    private int demoDataAmountMin = 2;
    private int demoDataAmountMax = 10;

    private InsightFieldConfiguration exampleField;

    public ImportConfiguration() {
        CacheProvider.getInstance()
                .setExpiration(isDemoData() ? 1 : cacheExpiration);

        // exampleField;
        this.exampleField = new InsightFieldTextConfiguration("exampleField", I18FactoryFake.getInstance()
                .getProperty("rlabs.module.i18n.import.module.exampleField"),
                isDemoData() ? I18FactoryFake.getInstance()
                        .getProperty("rlabs.module.i18n.import.module.demo") : I18FactoryFake.getInstance()
                        .getProperty("rlabs.module.i18n.import.module.exampleField.desc"));
        this.exampleField.setMandatory(true);

    }

    public String getExampleField() {
        return (String) this.exampleField.getValue();
    }

    public void setExampleField(String value) {
        this.exampleField.setValue(value);
    }

    public boolean isDemoData() {
        if (!demoData) {
            demoData = MavenOutputProperties.getInstance()
                    .getDemoMode();
        }
        return demoData;
    }

    public void setDemoData(boolean demoData) {
        this.demoData = demoData;
    }

    public int getDemoDataAmount() {
        return FakeDataProvider.getRandomNumber(this.demoDataAmountMin, this.demoDataAmountMax);
    }

    public void setDemoDataAmountMin(int demoDataAmountMin) {
        this.demoDataAmountMin = demoDataAmountMin;
    }

    public void setDemoDataAmountMax(int demoDataAmountMax) {
        this.demoDataAmountMax = demoDataAmountMax;
    }

    @Override
    public String toJSON() {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    @Override
    @JsonIgnore
    public List<InsightFieldConfiguration> getFieldsConfiguration() {
        return Arrays.asList(exampleField);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(exampleField.getValue(), exampleField.getValue());
    }
}
