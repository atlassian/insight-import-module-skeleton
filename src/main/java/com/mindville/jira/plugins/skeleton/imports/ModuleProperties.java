package com.mindville.jira.plugins.skeleton.imports;

import com.riadalabs.jira.plugins.insight.services.model.StatusTypeBean;

import java.util.HashMap;
import java.util.Map;

public class ModuleProperties {

    public enum Status {
        PENDING("PENDING", "Pending", StatusTypeBean.STATUS_CATEGORY_PENDING),
        RUNNING("RUNNING", "Running", StatusTypeBean.STATUS_CATEGORY_ACTIVE),
        SHUTTING_DOWN("SHUTTING_DOWN", "Shutting down", StatusTypeBean.STATUS_CATEGORY_PENDING),
        TERMINATED("TERMINATED", "Terminated", StatusTypeBean.STATUS_CATEGORY_INACTIVE),
        STOPPING("STOPPING", "Stopping", StatusTypeBean.STATUS_CATEGORY_PENDING),
        STOPPED("STOPPED", "Stopped", StatusTypeBean.STATUS_CATEGORY_INACTIVE),
        NOT_FOUND("NOT_FOUND", "Not Found", StatusTypeBean.STATUS_CATEGORY_INACTIVE),

        UNKNOWN("UNKNOWN", "Unknown", StatusTypeBean.STATUS_CATEGORY_INACTIVE);

        String source;
        String name;
        int category;
        private static Map<String, Status> operatorToEnumMapping;

        private Status(String source, String name, int category) {
            this.source = source;
            this.name = name;
            this.category = category;
        }

        public static Status getInstance(String source) {
            if (operatorToEnumMapping == null) {
                initMapping();
            }
            return operatorToEnumMapping.get(source);
        }

        private static void initMapping() {
            operatorToEnumMapping = new HashMap<>();
            for (Status s : values()) {
                operatorToEnumMapping.put(s.source, s);
            }
        }

        public String getName() {
            return name;
        }

        public int getCategory() {
            return category;
        }
    }

    // Reference Names
    public static final String REF_NAME_USING = "using";
    public static final String REF_NAME_ATTACHED = "attached";
    public static final String REF_NAME_CLONES = "clones";
    public static final String REF_NAME_BASED = "based";
    public static final String REF_NAME_INSTALLED = "installed";
    public static final String REF_NAME_CONTAINS = "contains";
    public static final String REF_NAME_BELONGS = "belongs";
    public static final String REF_NAME_CONNECTED = "connected";

    // ROOT Structure Items
    public static final String EXAMPLE_ROOT = "Example-Root";

    public static final String NAME = "Name";
    public static final String DESCRIPTION = "Description";

    public static final String STATUS = "Status";

    public static final String ID = "Id";
    public static final String TYPE = "Type";

    public static final String EXAMPLE = "Example";
    public static final String TEST = "Test";

}
