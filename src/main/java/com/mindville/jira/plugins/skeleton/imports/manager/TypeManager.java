package com.mindville.jira.plugins.skeleton.imports.manager;

import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportDataValues;

import java.util.List;

public interface TypeManager {

    List<DataLocator> getDataLocators();

    ImportDataValues getDataHolder();
}
