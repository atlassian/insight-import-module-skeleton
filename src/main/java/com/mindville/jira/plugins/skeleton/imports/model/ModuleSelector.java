package com.mindville.jira.plugins.skeleton.imports.model;

public enum ModuleSelector {
    /**
     * Selectors, added key selector for external use so we can change the selector names without breaking customers
     * configurations
     */

    REPLACE_ME("REPLACE_ME"),

    UNKNOWN("");

    private String selector;

    private ModuleSelector(String selector) {
        this.selector = selector;
    }

    public String getSelector() {
        return selector;
    }

    public synchronized static ModuleSelector getInstance(String operator) {

        ModuleSelector moduleSelector = null;
        for (ModuleSelector s : values()) {
            if (s.getSelector()
                    .equals(operator.toUpperCase())) {
                moduleSelector = s;
                break;
            }
        }

        return moduleSelector != null ? moduleSelector : ModuleSelector.UNKNOWN;
    }
}
