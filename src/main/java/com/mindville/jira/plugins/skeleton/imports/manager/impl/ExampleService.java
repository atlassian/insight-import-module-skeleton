package com.mindville.jira.plugins.skeleton.imports.manager.impl;

import com.mindville.jira.plugins.skeleton.imports.ImportConfiguration;
import com.mindville.jira.plugins.skeleton.imports.ModuleProperties;
import com.mindville.jira.plugins.skeleton.imports.manager.AbstractService;
import com.mindville.jira.plugins.skeleton.imports.manager.ClientProvider;
import com.mindville.jira.plugins.skeleton.imports.manager.DemoData;
import com.mindville.jira.plugins.skeleton.imports.manager.TypeManager;
import com.mindville.jira.plugins.skeleton.imports.model.skeleton.ModuleExample;
import com.mindville.jira.plugins.skeleton.imports.model.skeleton.SkeletonClient;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportDataValue;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportDataValues;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import io.riada.CacheProvider;
import io.riada.locators.JavaCodeDataLocator;
import io.riada.locators.ModuleDataLocator;
import io.riada.locators.ReferencedDataLocator;
import io.riada.locators.StandardDataLocator;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

import static io.riada.FakeDataProvider.GetFakeData;

// TODO: Replace me
// This is an example of an Service Class that is fetching module specific data
public class ExampleService extends AbstractService implements TypeManager, Supplier<List<ModuleExample>>, DemoData {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(ExampleService.class);

    public final static ModuleDataLocator name = new StandardDataLocator(ModuleProperties.NAME, "name");
    public final static ModuleDataLocator test = new StandardDataLocator(ModuleProperties.EXAMPLE, "test");

    private ModuleDataLocator[] dataLocators = new ModuleDataLocator[] {name, test};

    public ExampleService(ImportConfiguration configuration,
            List<DataLocator> configuredDataLocators,
            List<ModuleOTSelector> enabledModuleOTSelectors) {
        super(configuration, configuredDataLocators, enabledModuleOTSelectors);
    }

    @Override
    public List<DataLocator> getDataLocators() {
        return Arrays.asList(dataLocators);
    }

    @SuppressWarnings ("unchecked")
    @Override
    public ImportDataValues getDataHolder() {

        List<ImportDataValue> dataMap = new ArrayList<>();

        for (ModuleExample obj : getData()) {
            Map<DataLocator, List<String>> keyValueMap = new HashMap<>();
            for (ModuleDataLocator moduleDataLocator : dataLocators) {
                try {
                    if (!isDataLocatorConfigured(moduleDataLocator)) {
                        continue;
                    }
                    switch (moduleDataLocator.getLocatorType()) {
                        case JAVA_METHOD_VALUE:
                            keyValueMap.put(moduleDataLocator,
                                    ((StandardDataLocator) moduleDataLocator).getMethodValue(obj));
                            break;
                        case JAVA_CODE_VALUE:
                            keyValueMap.put(moduleDataLocator,
                                    ((JavaCodeDataLocator) moduleDataLocator).getCodeLocatorValue(obj));
                            break;
                        case REFERENCED_VALUE:
                            keyValueMap.put(moduleDataLocator,
                                    ((ReferencedDataLocator<ModuleExample, Object>) moduleDataLocator).getReferencedLocatorValue(
                                            obj, null));
                            break;
                        default:
                            break;
                    }
                } catch (Exception ee) {
                    logger.warn("Could not add value from locator: " + moduleDataLocator.getLocator(), ee);
                }
            }
            dataMap.add(new ImportDataValue(keyValueMap));
        }

        return ImportDataValues.create(dataMap);
    }

    public List<ModuleExample> getData() {
        List<ModuleExample> ret = new ArrayList<>();
        try {
            Object result = CacheProvider.getInstance()
                    .get(getCacheKey(), this);
            if (result != null) {
                ret = (List<ModuleExample>) result;
            }
        } catch (Exception ee) {
            logger.error("Error fetch ModuleExample data", ee);
        }

        return ret;
    }

    public List<ModuleExample> get() {
        logger.debug("Fetching ModuleExample Data from SkeletonClient.");

        if (configuration.isDemoData()) {
            return getDemoData();
        }

        List<ModuleExample> ret = new ArrayList<>();
        try {
            skeletonClient = (SkeletonClient) ClientProvider.INSTANCE.getClient(configuration);
            if (skeletonClient != null) {
                ModuleExample moduleExample = new ModuleExample();
                moduleExample.setTest(skeletonClient.getExampleData());
                try {
                    ret.add(moduleExample);
                } catch (Exception ee) {
                    logger.warn("Error adding ModuleExample: " + moduleExample.getName(), ee);
                }
            }
        } catch (Exception ex) {
            logger.error("Error fetch ModuleExample data", ex);
        }
        return ret.isEmpty() ? null : ret;
    }

    @Override
    public List<ModuleExample> getDemoData() {
        List<ModuleExample> ret = GetFakeData(ModuleExample.class, configuration.getDemoDataAmount());

        return ret;
    }
}
