package com.mindville.jira.plugins.skeleton.services;

public interface PropertyManager {

    String getLicenseKey();

    void setLicenseKey(String url);

}
