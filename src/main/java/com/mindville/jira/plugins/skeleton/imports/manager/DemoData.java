package com.mindville.jira.plugins.skeleton.imports.manager;

import java.util.List;

public interface DemoData<T> {

    List<T> getDemoData();

    List<T> getData();
}
