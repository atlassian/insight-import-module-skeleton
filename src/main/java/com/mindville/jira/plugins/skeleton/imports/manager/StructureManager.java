package com.mindville.jira.plugins.skeleton.imports.manager;

import com.mindville.jira.plugins.skeleton.imports.ImportConfiguration;
import com.mindville.jira.plugins.skeleton.imports.ModuleProperties;
import com.mindville.jira.plugins.skeleton.imports.manager.impl.ExampleService;
import com.mindville.jira.plugins.skeleton.imports.model.ModuleSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.DataLocator;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.MissingObjectsType;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ObjectTypeModuleExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.TemplateHandleMissingObjects;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.ThresholdType;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.IconExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.InsightSchemaExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectSchemaExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ObjectTypeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.ReferenceTypeExternal;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.StatusTypeExternal;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static io.riada.StructureUtils.addReferenceObjectTypeAttribute;
import static io.riada.StructureUtils.addTextObjectTypeAttribute;

public class StructureManager extends AbstractService {

    private final Logger logger = LoggerFactory.getLogger(StructureManager.class);

    private static final int OBJECT_SCHEMA_ID = 1; /* Just a fake id */
    private int objectTypeSequenceNumber;
    private int iconSequenceNumber;
    private int referenceTypeSequenceNumber;

    public StructureManager(ImportConfiguration configuration) {
        super(configuration, null, null);

        this.objectTypeSequenceNumber = 1;
        this.iconSequenceNumber = 1;
        this.referenceTypeSequenceNumber = 1;
    }

    public InsightSchemaExternal getPredefinedStructure() {

        InsightSchemaExternal insightSchemaExternal = new InsightSchemaExternal();

        ObjectSchemaExternal objectSchemaExternal = new ObjectSchemaExternal();
        insightSchemaExternal.setObjectSchema(objectSchemaExternal);
        objectSchemaExternal.setId(OBJECT_SCHEMA_ID);

        List<StatusTypeExternal> statusTypes = new ArrayList<>();
        for (ModuleProperties.Status status : ModuleProperties.Status.values()) {
            StatusTypeExternal statusTypeExternal = new StatusTypeExternal();
            statusTypeExternal.setName(status.getName());
            statusTypeExternal.setCategory(status.getCategory());
            statusTypeExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);
            statusTypes.add(statusTypeExternal);
        }

        insightSchemaExternal.setStatusTypes(statusTypes);

        /* How to handle missing objects update two days */
        TemplateHandleMissingObjects handleMissingObjectsUpdate =
                new TemplateHandleMissingObjects(MissingObjectsType.UPDATE);
        handleMissingObjectsUpdate.setThreshold(2);
        handleMissingObjectsUpdate.setThresholdType(ThresholdType.DAYS);
        handleMissingObjectsUpdate.setObjectTypeAttributeName(ModuleProperties.STATUS);
        handleMissingObjectsUpdate.setNewAttributeValue(ModuleProperties.Status.NOT_FOUND.getName());

        /* How to handle missing objects remove 1 day */
        TemplateHandleMissingObjects handleMissingObjectsRemoveOneDay =
                new TemplateHandleMissingObjects(MissingObjectsType.REMOVE);
        handleMissingObjectsRemoveOneDay.setThreshold(1);
        handleMissingObjectsRemoveOneDay.setThresholdType(ThresholdType.DAYS);

        /* How to handle missing objects remove 1 sync */
        TemplateHandleMissingObjects handleMissingObjectsRemoveOneSync =
                new TemplateHandleMissingObjects(MissingObjectsType.REMOVE);
        handleMissingObjectsRemoveOneSync.setThreshold(1);
        handleMissingObjectsRemoveOneSync.setThresholdType(ThresholdType.RUNS);

        /* EXAMPLE Root Object */
        ObjectTypeExternal rootObjectTypeExternal =
                appendObjectTypeExternal(ModuleProperties.EXAMPLE_ROOT, null, insightSchemaExternal, null, null, false,
                        true);
        objectSchemaExternal.getObjectTypes()
                .add(rootObjectTypeExternal);

        /* Example Object */
        ObjectTypeExternal exampleObjectTypeExternal =
                appendObjectTypeExternal(ModuleProperties.EXAMPLE, rootObjectTypeExternal, insightSchemaExternal,
                        ModuleSelector.REPLACE_ME, handleMissingObjectsRemoveOneSync, true, true);

        addExampleAttributes(exampleObjectTypeExternal);

        return insightSchemaExternal;
    }

    private void addExampleAttributes(ObjectTypeExternal objectType) {

        addTextObjectTypeAttribute(ModuleProperties.NAME, objectType, ExampleService.name, true, false, true);
        addTextObjectTypeAttribute(ModuleProperties.TEST, objectType, ExampleService.test);
    }

    private ObjectTypeModuleExternal appendObjectTypeExternal(String name,
            ObjectTypeExternal parentObjectTypeExternal,
            InsightSchemaExternal insightSchemaExternal,
            ModuleSelector selector,
            TemplateHandleMissingObjects handleMissingObjects,
            boolean createOTconfiguration,
            boolean enabledByDefault) {

        ObjectTypeModuleExternal objectTypeExternal =
                new ObjectTypeModuleExternal(selector != null ? selector.getSelector() : null, createOTconfiguration,
                        handleMissingObjects, !enabledByDefault);
        objectTypeExternal.setId(this.objectTypeSequenceNumber++);
        objectTypeExternal.setName(name);
        objectTypeExternal.setDescription("");
        objectTypeExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);

        IconExternal iconExternal = getIcon(name);
        iconExternal.setId(this.iconSequenceNumber++);
        insightSchemaExternal.getIcons()
                .add(iconExternal);
        objectTypeExternal.setIcon(iconExternal);

        if (parentObjectTypeExternal != null) {
            parentObjectTypeExternal.getObjectTypeChildren()
                    .add(objectTypeExternal);
        }
        return objectTypeExternal;
    }

    private IconExternal getIcon(String objectTypeName) {
        IconExternal iconExternal = new IconExternal();
        iconExternal.setName(objectTypeName);
        iconExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);

        InputStream in = StructureManager.class.getClassLoader()
                .getResourceAsStream("/images/icons/" + objectTypeName + "-48.png");

        try {
            iconExternal.setImage48(IOUtils.toByteArray(in));
            iconExternal.setImage16(iconExternal.getImage48());
        } catch (Exception ioe) {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
            }
        }

        return iconExternal;
    }

    private void addReferenceAttribute(InsightSchemaExternal insightSchemaExternal,
            String name,
            ObjectTypeExternal objectTypeExternal,
            ObjectTypeExternal referenceObjectTypeExternal,
            String referenceTypeName,
            boolean multiple,
            boolean includeChilds,
            DataLocator dataLocator,
            boolean identifier,
            String mappingIQL,
            String description) {

        addReferenceObjectTypeAttribute(insightSchemaExternal, name, objectTypeExternal, referenceObjectTypeExternal,
                referenceTypeName, multiple, includeChilds, dataLocator, identifier, mappingIQL, description,
                this.referenceTypeSequenceNumber++, OBJECT_SCHEMA_ID);
    }

    private ReferenceTypeExternal getReferenceTypeExternal(String name) {
        ReferenceTypeExternal referenceTypeExternal = new ReferenceTypeExternal();
        referenceTypeExternal.setName(name);
        referenceTypeExternal.setObjectSchemaId(OBJECT_SCHEMA_ID);
        return referenceTypeExternal;
    }

    public List<DataLocator> getDataLocators(ModuleOTSelector moduleOTSelector) {

        TypeManager typeManager = getDataTypeManager(moduleOTSelector);
        List<DataLocator> dataLocators = null;
        if (typeManager != null) {
            dataLocators = typeManager.getDataLocators();
        }

        if (dataLocators != null) {
            Collections.sort(dataLocators);
            return dataLocators;
        }

        return new ArrayList<>();
    }
}
