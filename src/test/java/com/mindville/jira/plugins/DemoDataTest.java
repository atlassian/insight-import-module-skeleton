package com.mindville.jira.plugins;

import com.mindville.jira.plugins.skeleton.imports.ImportConfiguration;
import com.mindville.jira.plugins.skeleton.imports.ImportModule;
import com.mindville.jira.plugins.skeleton.imports.manager.ClientProvider;
import com.mindville.jira.plugins.skeleton.imports.manager.StructureManager;
import com.mindville.jira.plugins.skeleton.imports.model.ModuleSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ImportDataHolder;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.ModuleOTSelector;
import com.riadalabs.jira.plugins.insight.services.imports.common.external.model.external.baseversion.InsightSchemaExternal;
import io.riada.CFPageContentGenerator;
import io.riada.CacheProvider;
import io.riada.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This Test-Class is using pure generated fake data from demo mode and can be used without the need of having a real
 * account
 */
public class DemoDataTest {

    private ImportConfiguration configuration = null;
    private ImportModule importModule = null;
    private ModuleOTSelector moduleOTSelector = null;
    private ImportDataHolder importDataHolder = null;

    @Before
    public void setUpClass() {
        try {
            configuration = new ImportConfiguration();
            configuration.setDemoData(true);
            configuration.setDemoDataAmountMin(2);
            configuration.setDemoDataAmountMax(10);

            importModule = new ImportModule(null, null, null, null);

            CacheProvider.getInstance()
                    .Reset();
            CacheProvider.getInstance()
                    .Clean();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    @Test
    public void validConfiguration() {
        Exception testEx = null;
        try {
            boolean valid = ClientProvider.INSTANCE.validClient(configuration);

            assertEquals(valid, true);
        } catch (Exception ee) {
            testEx = ee;
        }
        assertEquals(null, testEx);
    }

    @Test
    public void ImportTest() {
        Exception testEx = null;
        try {

            moduleOTSelector = new ModuleOTSelector(ModuleSelector.REPLACE_ME.getSelector());
            importDataHolder = importModule.dataHolder(configuration, moduleOTSelector);
            if (importDataHolder.numberOfEntries() > 0) {
                System.out.println(importDataHolder.asPrintableString(true));
            }

            /*
            for (ModuleSelector selector : ModuleSelector.values()) {
                moduleOTSelector = new ModuleOTSelector(selector.getSelector());
                try {
                    System.out.println("Testing: " + selector.name());
                    importDataHolder = importModule.dataHolder(configuration, moduleOTSelector);
                    if (importDataHolder.numberOfEntries() > 0)
                        System.out.println(importDataHolder.asPrintableString(true));
                } catch (Exception ex) {
                    testEx = ex;
                    System.out.println("Error testing: " + selector.name() + ex.getMessage());
                }
            }
            */
        } catch (Exception ee) {
            testEx = ee;
        }
        assertEquals(null, testEx);
    }

    @Test
    public void GenerateObjectDocumentation() {
        try {

            StructureManager awsStructureManager = new StructureManager(configuration);
            InsightSchemaExternal insightSchemaExternal = awsStructureManager.getPredefinedStructure();
            CFPageContentGenerator cfPageContentGenerator = new CFPageContentGenerator();
            String cfContent =
                    cfPageContentGenerator.GetConfluencePageContent(insightSchemaExternal.getObjectSchema(), true);

            if (!StringUtils.isNullOrEmpty(cfContent)) {
                System.out.println(cfContent);
                System.out.println("Content should be copied to clipboard, try to paste as markup.");
            }

        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    @After
    public void cleanUp() {
        CacheProvider.getInstance()
                .Reset();
        CacheProvider.getInstance()
                .Clean();
        configuration = null;
        importModule = null;
        moduleOTSelector = null;
        importDataHolder = null;
    }
}